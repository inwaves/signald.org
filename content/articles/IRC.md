---
title: IRC
---

[#signald on libera.chat](irc://irc.libera.chat/#signald) can be used to discuss signald in real time.

[**Click here to connect in a browser**](https://kiwiirc.com/nextclient/irc.libera.chat/#signald)

If you prefer to use Matrix, libera.chat operates a bridge: `#signald:libera.chat`