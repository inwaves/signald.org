---
title: Socket Protocol
aliases:
    - /articles/socket-protocol/
---

clients should communicate with signald via a unix socket that signald creates on startup. 

## Socket File Location

By default, clients should first try connecting to `$XDG_RUNTIME_DIR/signald/signald.sock`, and if it does not exist fall
back to `/var/run/signald/signald.sock`. However, clients should also allow the end user to specify a path to the socket file,
as some users like to put their socket file elsewhere.

While signald currently defaults to `/var/run/signald/signald.sock`, that will change in signald 0.15.0 (see
[#66](https://gitlab.com/signald/signald/-/issues/66)). Pass signald the `--user-socket` flag to use the new behavior.

## Messages

Messages sent to the socket should be valid JSON, terminating with a new line character (`\n`). Messages from signald will
follow the same format. The structure of requests that may be sent to signald are detailed in the [requests](/protocol/actions/v1/)
section, along with their response types. Responses may not be instant, especially for requests that require network operations.
To allow clients to track which response is associated with which request, a request ID may be specified in the `"id"` field of
all requests which will be returned in the `"id"` field of the response. For more information, see [Request IDs](/articles/protocol/request-ids/).

When a client connects to the socket, signald will send a [`JsonVersionMessage`](/protocol/structures/v1/JsonVersionMessage/),
to inform the client which version of signald is in use.